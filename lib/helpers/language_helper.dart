import 'package:flutter/material.dart';

class LanguageHelper extends ChangeNotifier {
  static const Locale kEnglishLocale = Locale('en', 'US');
  static const Locale kGermanLocale = Locale('de', 'DE');
  static const Locale kRussianLocale = Locale('ru', 'RU');

  static const int kLanguageEnglish = 1;
  static const int kLanguageGerman = 2;
  static const int kLanguageRussian = 3;

  //Add more helper methods later

}
