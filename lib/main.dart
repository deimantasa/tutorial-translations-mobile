import 'package:flutter/material.dart';
import 'package:tutorial_language_translator/pages/home_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

import 'helpers/language_helper.dart';

void main() {
  runApp(
    EasyLocalization(
      supportedLocales: [
        LanguageHelper.kEnglishLocale,
        LanguageHelper.kGermanLocale,
        LanguageHelper.kRussianLocale,
      ],
      path: 'assets/languages',
      child: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Language Translator Demo',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
				EasyLocalization.of(context).delegate,
      ],
			supportedLocales: EasyLocalization.of(context).supportedLocales,
      locale: EasyLocalization.of(context).locale,
      home: HomePage(),
    );
  }
}
