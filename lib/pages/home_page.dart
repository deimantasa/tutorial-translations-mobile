import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:tutorial_language_translator/helpers/language_helper.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("homePage.appBarTitle".tr()),
      ),
      body: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text("homePage.hello".tr()),
          RaisedButton(
            child: Text("homePage.selectLanguage".tr()),
            onPressed: () => _showSelectLanguageDialog(),
          )
        ],
      )),
    );
  }

  void _showSelectLanguageDialog() async {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            title: Text("homePage.dialogTitleSelectLanguage".tr()),
            children: <Widget>[
              RadioListTile(
                  title: Text("English"),
                  value: LanguageHelper.kEnglishLocale,
                  groupValue: 0,
                  onChanged: (_) {
                    EasyLocalization.of(context).locale = LanguageHelper.kEnglishLocale;
                    Navigator.pop(context);
                  }),
              Divider(height: 1),
              RadioListTile(
                  title: Text("Deutsch"),
                  value: LanguageHelper.kGermanLocale,
                  groupValue: 0,
                  onChanged: (_) {
                    EasyLocalization.of(context).locale = LanguageHelper.kGermanLocale;
                    Navigator.pop(context);
                  }),
              Divider(height: 1),
              RadioListTile(
                  title: Text("Pусский"),
                  value: LanguageHelper.kRussianLocale,
                  groupValue: 0,
                  onChanged: (_) {
                    EasyLocalization.of(context).locale = LanguageHelper.kRussianLocale;
                    Navigator.pop(context);
                  }),
            ],
          );
        });
  }
}
